/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Functional;

/**
 * Algumas funções genéricas para converter tipos especificos de vetores (arrays)
 * @author tiago
 */
public class TUtils {
    
    public static String[] convertIntArrToStrArr( int[] arr ) {
        String[] temp = new String[arr.length];
        for (int i=0;i<arr.length;i++) temp[i] = String.valueOf(arr[i]);
        return temp;
    } 
    public static String[] convertIntArrToStrArr( Integer[] arr ) {
        String[] temp = new String[arr.length];
        for (int i=0;i<arr.length;i++) temp[i] = String.valueOf(arr[i]);
        return temp;
    } 
    public static String[] convertObjectArrToStrArr( Object[] arr ) {
        String[] temp = new String[arr.length];
        for (int i=0;i<arr.length;i++) temp[i] = String.valueOf(arr[i]);
        return temp;
    } 
}