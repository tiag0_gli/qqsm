/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Functional;

import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 * Uma classe simples para controlo de erros.
 * Funcoes aqui presentes são chamadas em diversos outros modulos do programa,
 * de modo a economizar codigo e evitar redundância.
 */
public class ErrorHandler {
    
    public static void errorDB(SQLException ex) {
        
        JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO MySQL", 
                JOptionPane.ERROR_MESSAGE);
        
    }
    public static void errorGeneric(String msg) {
        JOptionPane.showMessageDialog(null, msg, "ERRO Generico", 
                JOptionPane.ERROR_MESSAGE);
    }
    
    public static void DBConnected() {
        
        JOptionPane.showMessageDialog(null, "DB Conetada", "INFO MySQL", 
                JOptionPane.INFORMATION_MESSAGE);
        
    }
    
    
}
