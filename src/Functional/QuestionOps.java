/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functional;

import Models.Question;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.swing.JOptionPane;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.util.Collections;
import java.util.List;

/**
 * Esta é uma classe adjacente à "Question" que possui funções que fazem a ligação
 * entre a estrutura de dados Question e o manuseio dos dados para posterior 
 * exibição.
 */
public class QuestionOps {
    
    public static void ShowQuestionTree(ArrayList<ArrayList<Question>> qal) {
        System.out.println("Coleção de Perguntas: " );
        for ( int QDifBookIdx=0; QDifBookIdx<qal.size();QDifBookIdx++ ) {
            System.out.println("    Coleção de Perguntas D:"+ String.valueOf(QDifBookIdx) );
                for ( int QIdx=0; QIdx<qal.get(QDifBookIdx).size(); QIdx++) {
                    System.out.println("      --------QIDX: " + String.valueOf(QIdx) + "-------------");
                    qal.get(QDifBookIdx).get(QIdx).printQuestion("      -->");
                }
        }
        
    }
    
    private static ArrayList<Question> _cropQuestionArrAndRandomize(ArrayList<Question> oldUncropped, int QAmtPerLevel) {
        List<Question> lq= oldUncropped;
        
        Collections.shuffle(lq);
        
        int endIdx = lq.size();
            
        for (int qRemIdx=QAmtPerLevel-1; qRemIdx<endIdx-1; qRemIdx++) {

            

            lq.remove(qRemIdx);
        }
            
        
        
        
        return (ArrayList<Question>) lq;
    }
    
    private static String[] _convertJSONArrToStringArr(JSONArray jarr) {
        String[] temp = new String[jarr.size()];
        Iterator<String> it = jarr.iterator();
        int i = 0;
        while ( it.hasNext() ) {
            temp[i] = it.next();
            i++;
        }
        return temp;
    }
    
    
    /**
     * Retorna uma Array 2D de Question s já de acordo com as cenas 
     * QAmtPerLevel/DifLvls.
     *
     * <p>A lista retornada é do tipo <b>Question[]</b></p>.
     *
     * @see #Question
     * @return Question[][]
     */
    public static ArrayList<ArrayList<Question>> loadQuestions(int QAmtPerLevel, int DifLvls) throws FileNotFoundException, IOException, ParseException {
        
        ArrayList<ArrayList<Question>> qtotal = new ArrayList<ArrayList<Question>>();
        FileReader fr = new FileReader("C:\\Users\\tiago\\Desktop\\Littlebitof\\perguntas.json");
        
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(fr);
        for(int d=0; d<DifLvls;d++) {  // d -> é a dificuldade do nivel || default: [dval->5]
            JSONArray arr = (JSONArray) obj.get("Difficulty" + String.valueOf(d+1));
            if (arr.size() < QAmtPerLevel) {
                return null;
            }
            Iterator<JSONObject> it = arr.iterator();
            ArrayList<Question> temp = new ArrayList<Question>();

            int qidx = 0;
            while (it.hasNext()) {  
                
                Object value = it.next();

                JSONObject n = new JSONObject((Map) value);

                JSONArray resps = (JSONArray)n.get("respostas");
                String[] respsStr = _convertJSONArrToStringArr(resps);


                temp.add(new Question(qidx,Integer.parseInt(n.get("correta").toString()) , d,n.get("pergunta").toString(), respsStr));
                qidx++;
            }
            qtotal.add(temp);
            qtotal.set(d, _cropQuestionArrAndRandomize(qtotal.get(d), QAmtPerLevel) );  
            
        
        }
        
        return qtotal;
    }

}
