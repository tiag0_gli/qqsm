/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 * Isto é uma classe-modelo que essencialmente empacota duas variaveis (1 String
 * , 1 int) sob o nome de PlayerScore.
 * @author tiago
 */
public class PlayerScore {
    public String nick;
    public int score;
    public PlayerScore(String nicky, int scory) {
        this.nick = nicky;
        this.score = scory;
    }
}
