/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 * Isto é uma classe instanciável que essencialmente empacota variaveis que dizem 
 * respeito a uma pergunta, tais como: [o indice obtido do json, o indice interno 
 * correto, a dificuldade, a pergunta em si e um vetor de respostas]
 * 
 * O construtor necessita que todas as variaveis acima sejam passadas pela ordem 
 * descrita.
 * 
 * Também existe um método que serve exclusivamente para debug, que mostra os valores
 * presentos na instancia.
 * 
 * @author tiago
 */
public class Question {
    public int index;    // indice obtido do json
    public int ci;       //correct index
    public int diff;
    public String content;
    public String[] answers;
    public Question(int i,int cidx,int d,String c,String[] a){
        this.index=i;
        this.ci=cidx;
        this.diff=d;
        this.content=c;
        this.answers=a;
        
    }      
    public void printQuestion(String ident) {
        System.out.println(ident + "JIndex : " + String.valueOf(this.index));
        System.out.println(ident + "JCIndex: " + String.valueOf(this.ci));
        System.out.println(ident + "Diff   : " + String.valueOf(this.diff));
        System.out.println(ident + "QTitle : " + String.valueOf(this.content));
        System.out.println(ident + "Answrs : " + String.valueOf(this.answers));
    }
}
