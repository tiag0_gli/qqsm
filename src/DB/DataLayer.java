/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DB;

import Functional.ErrorHandler;
import Models.PlayerScore;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.*;
import java.util.ArrayList;


/**
 *
 * @author tiago
 */
public class DataLayer {
    private static int MAXLIMIT = 1000;
    
    private static Manager globalManager = new Manager();
    public static int retriveMaxScoreByNick(String nick) {  // Retorna o score maximo dado um nick
        if (!globalManager.isConnected()) globalManager.Connect();
        
        try {
            ResultSet rs = globalManager.getConnection().prepareStatement("SELECT returnPlayerMaxScore('" + nick + "');", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery();
            if(!rs.first()) return -1;
            int mS = rs.getInt(1);
            rs.close();
            return mS;
           
        } catch (SQLException ex) {
            ErrorHandler.errorDB(ex);
        } finally {
            globalManager.Disconnect();
        }
        return -1;
    }
    
    public static boolean saveScore(String nicky, int newScore) {  // Salva um score na DB
        if (!globalManager.isConnected()) globalManager.Connect();
        try {
            boolean sucess = globalManager.getConnection().prepareStatement("CALL insertPlay('" + nicky + "', " + newScore + ");", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).execute();
            return sucess;
            
        } catch (SQLException ex) {
            ErrorHandler.errorDB(ex);
            return false;
        } finally {
            globalManager.Disconnect();
        }
        
    }
    
    public static ArrayList<PlayerScore> getTopBoard() {  // Retorna uma lista 2D do Top Board em tipo "ArrayList<PlayerScore>"
        ArrayList<PlayerScore> temp = new ArrayList<>();  // Isto é criar uma lista de objetos do tipo "PlayerScore"
        if (!globalManager.isConnected()) globalManager.Connect();
        try {
            ResultSet rs = globalManager.getConnection().prepareStatement("SELECT * FROM qqsmdb.returnbestscoreboard;", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery();
            
            while (rs.next()) {
                
                temp.add( new PlayerScore(rs.getString(1), rs.getInt(2)) );
                
            }
            
        } catch (SQLException ex) {
            ErrorHandler.errorDB(ex);
            return null;
        } finally {
            globalManager.Disconnect();
        }
        return temp;
    }
    
    /* 
     * Retorna por via de um chamamento de uma func mysql o valor do score mais baixo obitod em todas as plays
     */
    public static int getLowestScore() {  
        
        if (!globalManager.isConnected()) globalManager.Connect();
        try {
            ResultSet rs = globalManager.getConnection().prepareStatement("SELECT qqsmdb.getLowestScore();", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery();
            
            rs.next();
            int tr = rs.getInt(1);
            rs.close();
            return tr;
            
        } catch (SQLException ex) {
            ErrorHandler.errorDB(ex);
            return -1;
        } finally {
            globalManager.Disconnect();
        }
        
    }
    
    /* 
     * Retorna por via de um chamamento de uma func mysql o valor do score medio obito em todas as plays
     */
    public static int getAverageScore() {
        
        if (!globalManager.isConnected()) globalManager.Connect();
        try {
            ResultSet rs = globalManager.getConnection().prepareStatement("SELECT qqsmdb.calcAverageScoreAllPlaysInLimit("+String.valueOf(MAXLIMIT)+");", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery();
            
            rs.next();
            int tr = rs.getInt(1);
            rs.close();
            return tr;
            
        } catch (SQLException ex) {
            ErrorHandler.errorDB(ex);
            return -1;
        } finally {
            globalManager.Disconnect();
        }
        
    }
}
