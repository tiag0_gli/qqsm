/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DB;

/**
 *
 * @author tiago
 */

import Functional.ErrorHandler;
import com.mysql.cj.jdbc.Driver;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.*;


/**
* Esta classe controla as conexões com a DB.
* Tem metodos como: criar conexao, disconectar, verificar se conectado ...
*/
public class Manager {

    /**
     * @return the con
     */
    public Connection getConnection() {
        return con;
    }

    /**
     * @param con the con to set
     */
    public void setConnection(Connection con) {
        this.con = con;
    }
    private Connection con;
    
    public void Connect() {
        try {
            Connection newCon = DriverManager.getConnection("jdbc:mysql://localhost:3306/qqsmdb", "root", "2506");
            this.setConnection(newCon);
        } catch (SQLException ex) {
            
            ErrorHandler.errorDB(ex);
            
        }
        
    }
    
    public void Disconnect() {
        try {
            this.getConnection().close();
            this.setConnection(null);
            
        } catch (SQLException ex) {
            
            ErrorHandler.errorDB(ex);
            
        }
    }
    
    public boolean isConnected() {
        return this.getConnection() != null;
    }
    
}
