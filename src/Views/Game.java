/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Views;

import DB.DataLayer;
import Functional.ErrorHandler;
import Functional.QuestionOps;
import Functional.TUtils;
import Models.Question;
import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatLaf;
import com.formdev.flatlaf.FlatLightLaf;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionListener;
import org.json.simple.parser.ParseException;

/**
 *
 * @author tiago
 */
public class Game extends javax.swing.JFrame {

    
    
    
    //                                      VARIAVEIS GLOBAIS
    
    /*
    Aqui estão declaradas variaveis que inflenciam o comportamento do jogo, tais como:
    - o Numero de perguntas, numero de dificuldades, um vetor com os premios em dinheiro..
    */
    private int QMax = 10;  // QMax no minimo é 5, e acime segue 5*n, com n sendo natural;
    private int NumOfDifLevels = 5;
    private int QDifStep = QMax/NumOfDifLevels;
    private String Nick;
            
    private ArrayList<ArrayList<Question>> QArrayGlobal = new ArrayList<ArrayList<Question>>();
    private int QNum = 0;
    private Integer[] QEarnings = {
      500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000, 25000000
    };
    
    //                                      #################
    
    /* *couf* *couf* o construtor é uma função especial.
    * esta função é defenida dentro de uma classe e possui o mesmo nome da classe
    * ##
    * Eg: class Game {... 
    *   
    *   public Game () {
    *       <dentro do construtor>
    *   }
    * 
    * ...}
    * ##
    * Ao ser chamada -> new Game();
    * Irá executar o que tiver <dentro do construtor>.
    */
    
    /**
     * Este construtor inicializa coisas importantes:
     * 1º o tema pra fica bonit;
     * 2º organiza a lista de premios;
     * 3º pega o nick (que é passado como parametro no construtor) e pega da DB,
     * a partir do nick [usando queries para esse proposito], o masScore desse player;
     * 4º executa uma arvore de funções (SetupQuestionList) que tratam de pegar as perguntas,
     * organizar num modelo de dados para ser usadas com maior facilidade no decorrer do programa;
     * 5º apresenta a 1ª pergunta.
     * 
     */
    
    
    
    
    
    public Game(String nick) {
        //FlatLaf.setup(new FlatDarculaLaf());
        FlatLaf.setup(new FlatLightLaf());
        initComponents();
        SetupEarningsList();
        this.Nick = nick;
        lblNick.setText(this.Nick);
        int mxS = DataLayer.retriveMaxScoreByNick(this.Nick);
        if (mxS == -1) {
            ErrorHandler.errorGeneric("Ocorreu um erro interno. \n Secção #1.");
            System.exit(-1);
        }
        
        
        lblScoreHolder.setText("Score: " + mxS);
        SetupQuestionList();
        NextQuestion();
        
    }
   //<editor-fold desc="Aqui jaz todos os métodos usados para manipular dificuldade, avançar perguntas etc..">  
   private int getCurrentGameDifficulty() {
       
       return ( (int) Math.floor(QNum/QDifStep) );
       
   }
    private int getCurrentQNum(int cQ) {
        return (int) QNum%QDifStep;
    } 
    public void Answer(int ai) {
        int cQDif = getCurrentGameDifficulty();
        int cQNum = getCurrentQNum(cQDif);
        
        boolean correct = QArrayGlobal.get(cQDif).get(cQNum).ci == ai;
        if(correct && QNum+1 < QMax) {
            QNum++;
            NextQuestion();
            
        } else if (correct && QNum+1 == QMax) {
            DataLayer.saveScore(this.Nick, QNum);
            JOptionPane.showMessageDialog(null, "Parabens! Ganhaste o premio maximo: " + String.valueOf(QEarnings[QMax-QNum]) + "€");
            
            
            this.dispose();
            Main m = new Main();
            m.setVisible(true);
        } else {
            DataLayer.saveScore(this.Nick, QNum+1);
            JOptionPane.showMessageDialog(null, "Falhaste a pergunta e recebeste: " + String.valueOf(QEarnings[QMax-QNum]) + "€ :(");
            
            
            this.dispose();
            Main m = new Main();
            m.setVisible(true);
            
        }
    }
    private void __SetGameFields(String question, String[] ans) {
        highlightNextValue();
        txtAQHolder.setText(question);
        btnA0.setText(ans[0]);
        btnA1.setText(ans[1]);
        btnA2.setText(ans[2]);
        btnA3.setText(ans[3]);
    }
    //</editor-fold>
    
    //<editor-fold desc="Funcs para efeito visual na lista (highlight)">
    public void modifyBaseList() {  // Função para remover a propriedade "clicavel" do bagulho
        for(MouseListener me: listEarnings.getMouseListeners() ) {
            listEarnings.removeMouseListener(me);
        }
        
        for(MouseMotionListener ml: listEarnings.getMouseMotionListeners()) {
            listEarnings.removeMouseMotionListener(ml);
        }
        
    }
    
    public void highlightNextValue() {
        listEarnings.setSelectedIndex(QMax-QNum);
    }
    //</editor-fold>
    
    //<editor-fold desc="Funcs para setups e tals">
    public void SetupEarningsList() {
        modifyBaseList();
        List<Integer> qe = Arrays.asList(QEarnings);
        if (QEarnings[0] < QEarnings[1]) {
            Collections.reverse(qe);
        } 
        String[] strings =  TUtils.convertObjectArrToStrArr(qe.toArray());
        for (int x=0;x<strings.length;x++) strings[x] = strings[x] + "€";
        listEarnings.setModel(new javax.swing.AbstractListModel<String>() {
            
            @Override
            public int getSize() { return strings.length; }
            @Override
            public String getElementAt(int i) { return strings[i]; }
        });
    }
    
    public void SetupQuestionList() {
        ArrayList<ArrayList<Question>> qarr = null;  // Atribuido null pq a real atribu icao esta noutro escopo
                                 // e se ñ atribuir: null warning de não inicializado
        try {
            qarr = QuestionOps.loadQuestions(QDifStep, NumOfDifLevels);
            if (qarr == null) {
                JOptionPane.showMessageDialog(null, "ERRO: Não existem perguntas suficientes em cada nível:\n   - Min: " + String.valueOf(QDifStep));
                this.dispose();
            }
        } catch (Exception ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        } 
        QuestionOps.ShowQuestionTree(qarr);
        this.QArrayGlobal = qarr;
    }
    //</editor-fold>
    
    public void NextQuestion(){
        int cQDif = getCurrentGameDifficulty();
        int cQNum = getCurrentQNum(cQDif);
        System.out.println("cQDif: " + String.valueOf(cQDif));
        System.out.println("cQNum: " + String.valueOf(cQNum));
        // QuestionOps.ShowQuestionTree(QArrayGlobal);
        Question cq = QArrayGlobal.get(cQDif).get(cQNum);
        
        this.__SetGameFields(cq.content, cq.answers);
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnA2 = new javax.swing.JButton();
        btnA3 = new javax.swing.JButton();
        btnA0 = new javax.swing.JButton();
        btnA1 = new javax.swing.JButton();
        lblNick = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAQHolder = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listEarnings = new javax.swing.JList<>();
        lblScoreHolder = new javax.swing.JLabel();

        jLabel3.setText("5€");

        jLabel8.setText("5€");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(51, 102, 255));

        btnA2.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        btnA2.setText("holder2");
        btnA2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnA2ActionPerformed(evt);
            }
        });

        btnA3.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        btnA3.setText("holder3");
        btnA3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnA3ActionPerformed(evt);
            }
        });

        btnA0.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        btnA0.setText("holder0");
        btnA0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnA0ActionPerformed(evt);
            }
        });

        btnA1.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        btnA1.setText("holder1");
        btnA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnA1ActionPerformed(evt);
            }
        });

        lblNick.setText("holdernick");

        txtAQHolder.setEditable(false);
        txtAQHolder.setBackground(new java.awt.Color(51, 102, 255));
        txtAQHolder.setColumns(20);
        txtAQHolder.setFont(new java.awt.Font("Arial Black", 0, 12)); // NOI18N
        txtAQHolder.setForeground(new java.awt.Color(255, 255, 255));
        txtAQHolder.setRows(5);
        txtAQHolder.setText("QuestionHolder\n");
        txtAQHolder.setMaximumSize(new java.awt.Dimension(97, 36));
        jScrollPane1.setViewportView(txtAQHolder);

        jPanel2.setBackground(new java.awt.Color(42, 58, 240));

        listEarnings.setBackground(new java.awt.Color(42, 58, 240));
        listEarnings.setBorder(null);
        listEarnings.setForeground(new java.awt.Color(255, 255, 255));
        listEarnings.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "holderList" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        listEarnings.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        listEarnings.setFocusable(false);
        jScrollPane2.setViewportView(listEarnings);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        lblScoreHolder.setText("Score: holderscore");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(btnA0, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(46, 46, 46)
                            .addComponent(btnA1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(btnA2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(46, 46, 46)
                            .addComponent(btnA3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNick)
                    .addComponent(lblScoreHolder))
                .addGap(24, 24, 24)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnA0, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnA1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnA2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnA3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblNick)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblScoreHolder)))
                .addGap(57, 57, 57))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    //<editor-fold desc="Eventos de cada botão">
    private void btnA0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnA0ActionPerformed
        
        Answer(0);
        
    }//GEN-LAST:event_btnA0ActionPerformed

    private void btnA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnA1ActionPerformed
        Answer(1);
    }//GEN-LAST:event_btnA1ActionPerformed

    private void btnA2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnA2ActionPerformed
        Answer(2);
    }//GEN-LAST:event_btnA2ActionPerformed

    private void btnA3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnA3ActionPerformed
        Answer(3);
    }//GEN-LAST:event_btnA3ActionPerformed
    //</editor-fold>
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Game.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Game.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Game.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Game.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Game("DEBUG").setVisible(true);
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnA0;
    private javax.swing.JButton btnA1;
    private javax.swing.JButton btnA2;
    private javax.swing.JButton btnA3;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblNick;
    private javax.swing.JLabel lblScoreHolder;
    private javax.swing.JList<String> listEarnings;
    private javax.swing.JTextArea txtAQHolder;
    // End of variables declaration//GEN-END:variables
}
